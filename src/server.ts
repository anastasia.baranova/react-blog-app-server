import cors from 'cors';
import 'dotenv/config';
import express from 'express';

import { router } from './routes/posts';

const app = express();

app.use(
  cors({
    origin: 'http://localhost:3000',
  })
);

app.use(express.json());

app.use('/api/posts', router);

app.listen(process.env.PORT, () => {
  console.log('listening on port', process.env.PORT);
});
