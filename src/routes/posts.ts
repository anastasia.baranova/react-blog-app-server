import { PrismaClient } from '@prisma/client';
import express from 'express';
import { z, ZodError } from 'zod';

const createPostPayloadSchema = z.object({
  title: z.string(),
  body: z.string().max(20),
});

export const router = express.Router();

const prisma = new PrismaClient();

router.get('/', async (req, res, next) => {
  try {
    const posts = await prisma.post.findMany({
      include: { author: true },
    });

    res.json(posts);
  } catch (error) {
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const post = await prisma.post.findUnique({
      where: { id: req.params.id },
      include: { author: true },
    });

    res.json(post);
  } catch (error) {
    next(error);
  }
});

router.post('/', async (req, res) => {
  try {
    const post = await prisma.post.create({
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, id-denylist
      data: {
        ...createPostPayloadSchema.parse(req.body),
        // TODO: add authentication
        authorId: 'cl5qnnc8r0000uwuyfwz8h041',
      },
      include: { author: true },
    });

    res.json(post);
  } catch (error) {
    if (error instanceof ZodError) {
      res.status(400).json(error);
    } else {
      res.status(500).send('Unknown error occured');
    }
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const deletedPost = await prisma.post.delete({
      where: { id: req.params.id },
    });

    res.json(deletedPost);
  } catch (error) {
    next(error);
  }
});

router.put('/:id', async (req, res) => {
  try {
    const updatedPost = await prisma.post.update({
      where: { id: req.params.id },
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, id-denylist
      data: createPostPayloadSchema.parse(req.body),
      include: { author: true },
    });

    res.json(updatedPost);
  } catch (error) {
    if (error instanceof ZodError) {
      res.status(400).json(error);
    } else {
      res.status(500).send('Unknown error occured');
    }
  }
});
