import { PrismaClient } from '@prisma/client';

import { posts } from './posts';
import { users } from './users';

const prisma = new PrismaClient();

const getRandomElement = <T>(array: T[]) => {
  const randomIndex = Math.floor(Math.random() * array.length);

  return array[randomIndex];
};

const main = async () => {
  const usersPromises = users.map((user) =>
    prisma.user.create({
      // eslint-disable-next-line id-denylist
      data: user,
    })
  );

  const createdUsers = await Promise.all(usersPromises);

  const postsPromises = posts.map((post) =>
    prisma.post.create({
      // eslint-disable-next-line id-denylist
      data: {
        ...post,
        authorId: getRandomElement(createdUsers).id,
      },
    })
  );

  await Promise.all(postsPromises);
};

main()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  })
  .finally(async () => prisma.$disconnect());
